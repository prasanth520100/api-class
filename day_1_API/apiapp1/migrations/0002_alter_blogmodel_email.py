# Generated by Django 5.0.2 on 2024-03-19 06:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apiapp1', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogmodel',
            name='email',
            field=models.EmailField(max_length=254),
        ),
    ]
