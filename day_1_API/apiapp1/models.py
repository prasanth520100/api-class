from django.db import models


from django.contrib.auth.models import AbstractUser

#import slug
from django.utils.text import slugify

from django.contrib.auth.models import AbstractUser



class User(AbstractUser):
    
    USER_ROLE = [("admin","admin"),("moderator","moderator"),("normal_user","normal_user")]

    email = models.EmailField(unique=True)
    phone_number = models.IntegerField()
    address = models.TextField()

    role = models.CharField(choices=USER_ROLE,max_length=20)

     # to login with email 

    REQUIRED_FIELDS = ["phone_number"]  # to create super user


    















class BlogModel(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='blogs')     
    title = models.CharField(max_length=100) 
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at= models.DateTimeField(auto_now=True)
    slug = models.SlugField(unique = True)
    email = models.EmailField()
    username = models.CharField(max_length=100)

    def __str__(self):
        return self.user.username

    

    # convert title into slug

    def save(self,*args,**kwargs):
        self.slug =slugify(self.title)  
        super().save()







class Author(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)










