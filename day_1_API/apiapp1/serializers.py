from .models import Author, BlogModel, Book
from django.contrib.auth.models import User

from rest_framework import serializers




class BlogSerializer(serializers.ModelSerializer):

    #syntax: user_defined_variable = serializers.SerializerMethodField()
    # username = serializers.SerializerMethodField()
    # email = serializers.CharField(source="user.email")
    # username = serializers.CharField(source="user.username")

    # user = serializers.CharField(source="user.username")
    
    class Meta:
        model = BlogModel   
        fields = ["id","title","description"]
        # fields="__all__"

    #get_ ---> built in method for serializerMethodField for creating method(function)
    #object ---> represent the data of my model , example : BlogModel ,object means data
    #example :
    #   object = BlogModel
                
    # def get_username(self,object):
    #     return f'{object.user.username} 1234'












    def create(self,validated_data):
        print("create methods works:::::::: 2")
        validated_data["user"] =self.context["request"].user
        return BlogModel.objects.create(**validated_data)
    



    # def update(self,instance,validated_data):
    #     print("serializer update methoid works ")
    #     instance.title = validated_data.get("title")
    #     instance.description = validated_data.get("description")
    #     instance.save()
    #     return instance

        

   










    #object level validation
    # def validate_title(self,value):

    #     if value != "" and value == "":
    #         print("validate methods works:::::::: 3")

    #         raise serializers.ValidationError("u cant write title")
    #     return value




class LoginSer(serializers.ModelSerializer):
    username = serializers.CharField(source="author.username")
    password = serializers.CharField(source="author.password")

    class Meta:
        model = User
        fields = ["username","password"]














