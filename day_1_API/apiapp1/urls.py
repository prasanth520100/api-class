
from django.urls import path

from apiapp1 import views

urlpatterns = [
    path('index',views.index,name='index'),
    path("get-data/<str:slug>",views.get_data,name="get_data"),
    path("post-data",views.blog_post,name="post-data"),

    #update
    path("update-put/<int:pk>",views.update_data,name="put-method"),


    #blog




    #lofin
    path("login/",views.login_view,name="login"),
    path("logout/",views.logout_view,name="logout"),




    #emil
    path("email-send/",views.email_sending,name="email-send"),


    #searc
    path("searchdata/",views.query_filter_view,name="search"),





    #cbv 
    # path("blogs/",views.BlogCreateView.as_view(),name="create-blog"),
    path("blogdata/",views.BlogListData.as_view(),name="get-all-data"),
    path("bloggs/<str:slug>",views.RetriveBlogAPIView.as_view()),
    path("delete-blog/<str:slug>",views.DeleteBlogAPI.as_view(),name="delete-blog"),
    path("update/<str:slug>/",views.BlogUpdateAPIView.as_view(),name="update-blog"),
    path("blogs/",views.BlogListCreateAPIView.as_view(),name="list-create-blog"),
    path("blogs/<str:slug>/",views.BlogRetrieveUpdateAPIView.as_view(),name="blogs-update"),
    path("blogss/<str:slug>/",views.BlogRetrieveDestroyAPIView.as_view(),name="delete-retrieve"),

    path("blogsss/<str:slug>/",views.BlogRetrieveUpdateDestroyAPIView.as_view(),name="rdu")
]   
