import random
from django.shortcuts import render,HttpResponse



from rest_framework.decorators import api_view



from rest_framework.response import Response

from .serializers import BlogSerializer,LoginSer

from .models import BlogModel



# importing status module
from rest_framework import status
from .models import User



#login,logout ,authenticate
from django.contrib.auth import authenticate,login,logout




from rest_framework.views import APIView
from rest_framework.decorators import api_view, authentication_classes,permission_classes
from rest_framework.response import Response


from rest_framework.authentication import  BasicAuthentication
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly



#email
from django.core.mail import send_mail
from django.conf import settings




#Q
from django.db.models import Q




#cbv

from rest_framework.generics import (CreateAPIView,
                                      ListAPIView,
                                      RetrieveAPIView,
                                      DestroyAPIView,
                                      UpdateAPIView,
                                      ListCreateAPIView,
                                      RetrieveUpdateAPIView,
                                      RetrieveDestroyAPIView,
                                      RetrieveUpdateDestroyAPIView,
                                      )



@api_view(["GET"])

@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def index(request):

    if request.user.role != "admin":

        blog_db = BlogModel.objects.all()

  
    # filter_data = BlogModel.objects.get(title__icontains=search,desc)


    
    #---> fetch data from db --> convert into python dict --> convert into json format 
        serializer = BlogSerializer(blog_db,many=True)

    #---> send the json data to the client    
        return Response(serializer.data,status=status.HTTP_200_OK)
    else:
        return Response({"Message":"You are not an admin "})

@api_view(["GET"])
def get_data(request,slug):
    my_data = BlogModel.objects.get(slug=slug)
    serializer = BlogSerializer(my_data)
    

    return Response({"Message":serializer.data})






def generate_otp():
    number = random.randint(1,5)
    return number




@api_view(["POST"])
def blog_post(request):
    serializer = BlogSerializer(data = request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({"Message":serializer.data ,"data":my_ser.data},status=status.HTTP_201_CREATED)
    else:
        return Response({"Message":serializer.errors})






#

#update 
 #1.PUT
 #2.PATCH -- partial update 



@api_view(["PUT","PATCH","DELETE"])
def update_data(request,pk):

    my_data = BlogModel.objects.get(id=pk)


    #for put
    if request.method == "PUT":
        my_serializer = BlogSerializer(my_data ,data=request.data)

        if my_serializer.is_valid():
            my_serializer.save()
            return Response({"Message":"Your Data has updated (PUT)"})
        
        else:
            return Response(my_serializer.errors)

    #for patch/
    elif request.method == "PATCH":
        my_serializer = BlogSerializer(my_data,data=request.data,partial=True)

        if my_serializer.is_valid():
            print("patch first works")
            my_serializer.save()
            return Response({"Message":"Your Data has updated (PATCH)"})
        
        else:
            return Response(my_serializer.errors)


    #delete
    else:
        my_data.delete()
        return Response({"Message":"Your Data has delted (DELETE)"},status=status.HTTP_204_NO_CONTENT)
        






@api_view(['POST'])
def login_view(request):
    # Extract username and password from request data
    username = request.data.get('username')
    password = request.data.get('password')

    # Authenticate user
    user = authenticate(username=username, password=password)

    # Check if authentication was successful
    if user is not None:
        # If user is authenticated, log them in
        login(request, user)
        return Response({'message': 'Login successful'}, status=status.HTTP_200_OK)
    else:
        # If authentication failed, return an error response
        return Response({'error': 'Login Failed '}, status=status.HTTP_401_UNAUTHORIZED)






@api_view(["GET"])
def logout_view(request):
    logout(request)
    return Response({"Message":"Logout success"})





@api_view(["POST"])
def email_sending(request):
    if request.method == "POST":
        serializer = BlogSerializer(data=request.data)

        # request.post.get() --- for django retrive data
        # request.data.get()  --- for django rest api retrive data
        
        if serializer.is_valid():

            email = serializer.validated_data.get("email")
            username = serializer.validated_data.get("username")

            msg = """ Certainly! Exploring the wildlife in Madagascar is a fantastic adventure. Here's a budget-friendly plan for a one-week trip to Madagascar:

                    Day 1-2: Antananarivo

                    Arrival in Antananarivo (Tana):

                    Fly into Ivato International Airport.
                    Explore the capital city, Antananarivo, visit the Royal Hill of Ambohimanga and the Lemurs' Park.
                    Local Cuisine and Accommodation:

                    Try local Malagasy dishes at affordable eateries.
                    Opt for budget accommodation options like guesthouses or budget hotels.
                    **Day 3-4: Andasibe-M """

            send_mail(subject="django rest api",
                    message=msg,
                    from_email=settings.EMAIL_HOST_USER,
                    recipient_list=[email],
            )

            serializer.save()
            return Response({"Message":"Post creatd and email has sent"})
        else:
            return Response(serializer.errors)






@api_view(["GET"])
def query_filter_view(request):

    #  | -----> OR
    # &  --------> AND
    # ~ -------> negotion NOT

    if request.method == "GET":
        
        search_data = request.query_params.get("search")   

        filter_data = BlogModel.objects.filter(~Q(title__icontains=search_data))
        serializer = BlogSerializer(filter_data,many=True) 

        #get -- > dont have to use many=True inside the serializer
        #filter() or all() ___> should use many=True
        return Response(serializer.data)


 




#cbv

class BlogCreateView(CreateAPIView):
    serializer_class = BlogSerializer
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]


    
    # def post(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception = True)


    #     serializer.save()
    #     return self.create(request, *args, **kwargs)





 







class BlogListData(ListAPIView):
    serializer_class = BlogSerializer
    queryset = BlogModel.objects.all()



    def get_queryset(self):
        search = self.request.query_params.get("search")
        # username = self.request.data.get("username")
        # username = "poiuytrewq"
        dummy = BlogModel.objects.filter(titile__icontins=search)
        return dummy





class RetriveBlogAPIView(RetrieveAPIView):
    queryset = BlogModel.objects.all()
    serializer_class = BlogSerializer
    lookup_field = "slug"

    
    # lookup_field = "slug"

    #.get()
    # pk - built in 








    # def get_querset(self,request,pk):
    #     return self.queryset.get(pk=pk)




class DeleteBlogAPI(DestroyAPIView):
    queryset = BlogModel.objects.all()
    lookup_field = "slug"



class BlogUpdateAPIView(UpdateAPIView):
    queryset = BlogModel.objects.all()
    serializer_class = BlogSerializer
    lookup_field = 'slug'





#ListCreateAPIViw

class BlogListCreateAPIView(ListCreateAPIView):
    queryset = BlogModel.objects.all()
    serializer_class = BlogSerializer
    




#RetriveUpdateAPIView


class BlogRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    queryset = BlogModel.objects.all()
    serializer_class = BlogSerializer
    lookup_field  = 'slug' 




#RetrieveDestroyAPIView

class BlogRetrieveDestroyAPIView(RetrieveDestroyAPIView):
    queryset = BlogModel.objects.all()
    serializer_class = BlogSerializer
    lookup_field = "slug"











#RetrieveUpdateDestroyAPIView
class BlogRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = BlogModel.objects.all()
    serializer_class = BlogSerializer
    lookup_field = "slug"

























### 

''' 
{
    "title": "django tutorial 4",
    "description": "django tutorial 4"
}

'''






