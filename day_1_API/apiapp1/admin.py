from django.contrib import admin


from .models import Author, BlogModel, Book, User




class BlogAdmin(admin.ModelAdmin):
    list_display = ["id","user","title","description"]


admin.site.register(BlogModel,BlogAdmin)

class AuthorAdmin(admin.ModelAdmin):
    list_display = ["id","name"]

admin.site.register(Author,AuthorAdmin)


class BookAdmin(admin.ModelAdmin):
    list_display = ["id","title","author"]

admin.site.register(Book,BookAdmin)





class UserAdmin(admin.ModelAdmin):
    list_display = ["id","username","email","phone_number","role"]

admin.site.register(User,UserAdmin)



